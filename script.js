// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.


function sendRequest(url, method, options) {
  return fetch(url, {method: method, ...options}).then(response => {
      if (response.ok) {
          if (response.headers.get("content-length") === "0" || !response.headers.get("content-type").startsWith("application/json")) {
              return null;
          } else {
              return response.json();
          }
      } else {
          throw new Error('Something went wrong');
      }
  })
}
const IP = 'https://api.ipify.org/?format=json'
const userInfo = 'http://ip-api.com/json/'

const res = document.querySelector('#result');
async function getUsersIP() {
    const ip = await sendRequest(IP, "GET").then((data) => {
        return data.ip;
    });

    const location = await sendRequest(`${userInfo}${ip}`, "GET");

    res.innerHTML = `
        <p>IP: ${location.query}</p>
        <p>Country: ${location.country}</p>
        <p>City: ${location.city}</p>
        <p>Region: ${location.regionName}</p>
        <p>ISP: ${location.isp}</p>
    `
}

document.querySelector('#search').addEventListener('click', getUsersIP);






